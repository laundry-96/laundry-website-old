import jinja2
import os
import shutil

os.chdir("template")
to_render = []
for root, dirs, files in os.walk("."):
    for name in files:
        html_file = os.path.join(root, name)

        if "html" in html_file:
            to_render.append(html_file)

os.chdir("..")


jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader('template'))
for html_file in to_render:
    rendered_file = os.path.join("rendered", html_file)
    template = jinja_env.get_template(html_file)
    if not os.path.exists(os.path.dirname(rendered_file)):
        try:
            os.makedirs(os.path.dirname(rendered_file))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    with open(rendered_file, "w") as rendered_data:
        rendered_data.write(template.render())

shutil.copy("template/common.css", "rendered")
